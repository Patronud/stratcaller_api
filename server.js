var http = require('http');
var request = require('request');
var express = require('express');
var app = express();

app.set('port', process.env.OPENSHIFT_NODEJS_PORT || 8080);
app.set('ip', process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1');

var session = require('express-session');
var FirebaseStore = require('connect-firebase')(session);
var firebaseStoreOptions = {
    // Your FireBase database
    host: 'resplendent-inferno-5842.firebaseio.com/', // Secret token you can create for your Firebase database
    token: 'U8SRp6l3eDLM13DR8JREc5Im87AcVstjS82OS8XV', // How often expired sessions should be cleaned up
    reapInterval: 600000
, };

app.use(session({
    store: new FirebaseStore(firebaseStoreOptions)
    , secret: 'cakes', // Change this to anything else
    resave: false
    , saveUninitialized: true
}));

var OpenIDStrategy = require('passport-openid').Strategy;
var SteamStrategy = new OpenIDStrategy({
        // OpenID provider configuration
        providerURL: 'http://steamcommunity.com/openid'
        , stateless: true, // How the OpenID provider should return the client to us
        returnURL: 'http://stratcaller-patronud.rhcloud.com/auth/openid/return'
        , realm: 'http://stratcaller-patronud.rhcloud.com'
    , }
    , function (identifier, done) {
        process.nextTick(function () {
            // Retrieve user from Firebase and return it via done().
            var user = {
                identifier: identifier, // Extract the Steam ID from the Claimed ID ("identifier")
                steamId: identifier.match(/\d+$/)[0]
            };
            return done(null, user);
        });
    });
var passport = require('passport');
passport.use(SteamStrategy);
passport.serializeUser(function (user, done) {
    done(null, user.identifier);
});

passport.deserializeUser(function (identifier, done) {
    done(null, {
        identifier: identifier
        , steamId: identifier.match(/\d+$/)[0]
    });
});
app.use(passport.initialize());
app.use(passport.session());

app.post('/auth/openid', passport.authenticate('openid'));

// -----------------------REDIRECTS AFTER LOGIN-------------------------------
app.get('/auth/openid/return', passport.authenticate('openid')
    , function (request, response) {
        if (request.user) {
            response.redirect('/');
            //response.redirect('/?steamid=' + request.user.steamId);
        } else {
            response.redirect('/?failed');
        }
    });

app.post('/auth/logout', function (request, response) {
    request.logout();
    // After logging out, redirect the user somewhere useful.
    // Where they came from or the site root are good choices.
    response.redirect(request.get('Referer') || '/')
});

// ```
//
// Lastly we need a page!  We'll just write some dirty HTML to get something
// on the screen:
//
// ```js

app.get('/', function (request, response) {
    response.writeHead(200, {
        'Content-Type': 'text/html'
    });
    response.write('<!DOCTYPE html>')
    if (request.user) {
        response.write(request.session.passport &&
            JSON.stringify(request.user) || 'None');
        response.write('<form action="/auth/logout" method="post">');
        response.write('<input type="submit" value="Log Out"/></form>');
        response.write(request.user.steamId);
        friends(request.user.steamId);
        yourStats(request.user.steamId);
        request.session.userdata = request.user;
        request.session.playerstatus = "online";
    } else {
        if (request.query.steamid) {
            response.write('Not logged in.');
        }
        response.write('<form action="/auth/openid" method="post">');
        response.write(
            '<input name="submit" type="image" src="http://steamcommunity-a.' +
            'akamaihd.net/public/images/signinthroughsteam/sits_small.png" ' +
            'alt="Sign in through Steam"/></form>');
    }
    response.send();
});

// ------------------------ our code ---------------------------------

app.get('/userdata', function (request, response) {
    //  response.send(request.session.userdata.steamId + " " + request.session.playerstatus);
    var id = "76561197981045695";
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "http://api.steampowered.com/ISteamUserStats/GetUserStatsForGame/v0002/?appid=730&key=9E9964DCD224DA403FF5EDE77865C6D5&steamid=" + id, false);
    xhr.send();
    console.log(xhr.status);
    console.log(xhr.statusText);
    response.send(request.session.userdata);
});


app.get('/test/:id', function (req, response) {
       yourStats(req, response);
    console.log(req.params.id);
  //  playerInfo(response);
});

function yourStats(req, response) {
    //fjern og brug tilsendt id
    var id = "76561197978856439";
    console.log('before');
    request.get(
        'http://api.steampowered.com/ISteamUserStats/GetUserStatsForGame/v0002/?appid=730&key=9E9964DCD224DA403FF5EDE77865C6D5&steamid=' + req.params.id
        , function (error, res, body) {
            var obj = JSON.parse(body);
            var playerStats = {
                "total_kills": ""
                , "total_deaths": ""
                , "total_kills_headshot": ""
                , "total_time_played": ""
                , "total_mvps": ""
                , "total_planted_bombs": ""
                , "total_defused_bombs": ""
                , "total_wins": ""
                , "total_rounds_played": ""
            }
            for (var stat in obj.playerstats.stats) {
                switch (obj.playerstats.stats[stat].name) {
                case "total_kills":
                    playerStats.total_kills = obj.playerstats.stats[stat].value;
                    break;
                case "total_deaths":
                    playerStats.total_deaths = obj.playerstats.stats[stat].value;
                    break;
                case "total_kills_headshot":
                    playerStats.total_kills_headshot = obj.playerstats.stats[stat].value;
                    break;
                case "total_time_played":
                    playerStats.total_time_played = obj.playerstats.stats[stat].value;
                    break;
                case "total_mvps":
                    playerStats.total_mvps = obj.playerstats.stats[stat].value;
                    break;
                case "total_planted_bombs":
                    playerStats.total_planted_bombs = obj.playerstats.stats[stat].value;
                    break;
                case "total_defused_bombs":
                    playerStats.total_defused_bombs = obj.playerstats.stats[stat].value;
                    break;
                case "total_wins":
                    playerStats.total_wins = obj.playerstats.stats[stat].value;
                    break;
                case "total_rounds_played":
                    playerStats.total_rounds_played = obj.playerstats.stats[stat].value;
                    break;

                }
                //                if (obj.playerstats.stats[stat].name == "total_kills") {
                //                    playerStats.total_kills = obj.playerstats.stats[stat].value;
                //                } 
            }

    request.get(
        "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=9E9964DCD224DA403FF5EDE77865C6D5&steamids=" + req.params.id
        , function (error, res, body) {
            var obj2 = JSON.parse(body);
        var obj2 = obj2;
            response.writeHead(200, {
                'Content-Type': 'application/json'
                , 'Access-Control-Allow-Origin': '*'
            });
//            for (var attrname in playerStats) { obj2[attrname] = playerStats[attrname]; }
   var obj3 = {};
    for (var attrname in playerStats) { obj3[attrname] = playerStats[attrname]; }
    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
    
            response.write(JSON.stringify(obj3));
            response.send();
        })
        }
    )
};

    //        var playerInfo = {
      //          "steamid" = obj.response.players[0].steamid
    //            , "profilestate": obj.response.players[0].steamid
    //            , "personaname": obj.response.players[0].steamid
    //            , "avatarfull": obj.response.players[0].steamid
    //            , "personastate": obj.response.players[0].steamid}}
        
 //        playerInfo.steamid = obj.response.players[0].steamid;
   //         playerInfo.profilestate = obj.response.players[0].profilestate;
     //       playerInfo.personaname = obj.response.players[0].personaname;
       //     playerInfo.avatarfull = obj.response.players[0].avatarfull;
         //   playerInfo.personastate = obj.response.players[0].personastate;
        
function friends(id) {
    request.get(
        // gets the friendslist of the user that logs in
        'http://api.steampowered.com/ISteamUser/GetFriendList/v0001/?key=9E9964DCD224DA403FF5EDE77865C6D5&steamid=' + id
        , function (error, response, body) {
            var obj = JSON.parse(body)
            for (var friend in obj.friendslist.friends) {
                var id = obj.friendslist.friends[friend].steamid;
                games(id, function (userID) {
                    personas(userID, function (player) {
                        //          console.log(player.personaname, player.steamid, player.avatar);
                    });
                });
            }
        }
    );
}

function personas(id, callback, err) {

    request.get(
        // gets the player summeries and passes it all with the player callback to the friends function:
        // everything can be accessed with: player.steamid
        // attributes steamid, communityvisibilitystate, profilestate, personaname, lastlogoff, profileurl
        // avatar, avatarmedium,avatarfull, personastate, realname, primaryclanid, timecreated, personastateflags
        'http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=9E9964DCD224DA403FF5EDE77865C6D5&steamids=' + id + '&format=json'
        , function (error, response, body) {
            var obj = JSON.parse(body);
            if (obj.response.players[0]) {
                var player = obj.response.players[0];
                callback(player);
            }
        }

    );
}

function games(id, callback, err) {
    // gets the owned games of the current id and if it has the wanted game it callbacks with the id
    request.get(
        'http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=9E9964DCD224DA403FF5EDE77865C6D5&steamid=' + id + '&format=json'
        , function (error, response, body) {
            var obj = JSON.parse(body);
            for (var game in obj.response.games) {
                // works with any steam game id ex banished 242920, cs:go 730
                if (obj.response.games[game].appid == "730") {
                    callback(id);
                }
            }
        }
    );
}
//http://api.steampowered.com/ISteamUserStats/GetUserStatsForGame/v0002/?appid=730&key=9E9964DCD224DA403FF5EDE77865C6D5&steamid=76561197978856439
// get user data in csgo from steam (substanceD)
//http://api.steampowered.com/ISteamUserStats/GetUserStatsForGame/v0002/?appid=730&key=9E9964DCD224DA403FF5EDE77865C6D5&steamid=76561197964501258
// BOT Bauer's
// Start the server
//
// ```js
var ref = new Firebase("https://resplendent-inferno-5842.firebaseio.com");
//Writting new data to the database
app.get('/users', function (request, response) {
    var usersRef = ref.child("users");
    usersRef.set(
        request.session.userdata
    );
});

app.get('/lobby', function (request, response) {
    var lobbiesRef = ref.child("lobbies");
    lobbiesRef.set(
        request.session.userdata.steamId
    );

    lobbiesRef.child(request.session.userdata.steamId).push({
        id: "friendId 1"
        , accepted: false
    });
    lobbiesRef.child(request.session.userdata.steamId).push({
        id: "friendId 2"
        , accepted: false
    });
});


http.createServer(app).listen(app.get('port'), app.get('ip'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});